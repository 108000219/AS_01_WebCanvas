# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**         | **Score** | **Check** |
|:-------------------------------- |:---------:|:---------:|
| Enable user download with background                                 |     1~5%      |  Y         |
| Changing canvas background color |   1~5%    |     Y     |


---

### How to use 

My webpage:
![](https://i.imgur.com/vOCniMW.jpg)
1. Click the buttons in "Brush Type" to change your brush shape and mode.
2. Change your color of pen using the color selector above.
3. Change your curve width using the select bar in "Line Width".
4. Customize your font with the drop-down menu in "Font".
5. Decide if you want to change the background color of the canvas using the checkbox below.(if the checkbox is checked, you can select your background color via cursor using the color selector above)
6. Decide if you want to have your downloaded image with the background using the checkbox "Download with Background Color".
7. Press the Choose File/undo/redo/download/clear to import a file/undo/redo/download current image on the canvas/clean the whole canvas.

### Function description
1. init() initialize the settings.
2. updatelinewidth(val) change the curve width to val.
3. changeBrush(num) change the drawing mode to num.
4. push() push the current image on the canvas in an array.
5. draw(e) change the cursor icon and draw the graph when the mouse is moving on the canvas.
6. Mousedown(e) get the cursor information on mousedown.
7. Mouseup() push the current image to the array on mouse up and change the drawingMode to false.
8. Mouseout() push the current image to the array on mouse up and change the drawingMode to false.
9. Mouseclick(e) deal with the textbox mode.
10. drawText(txt,x,y) draw the text you type in the textbox onto the canvas.
11. undo() undo the step
12. redo() redo the step
13. onclear() clear the canvas
14. addBackgnd() draw the background color on the background if the user want to download image with background
15. onsave() save the current image
16. LoadImg(e) import file to the canvas
17. Draw() the function to help the LoadImg function
18. failed() alert when the user failed to load the image
19. mousemoveStrip(e) change the color when the mouse is moving on the color selector
20. mouseclickStrip(e) change the color in the color selector when the mouse click on the strip
21. mouseupStrip() change the color in the color selector when the mouse click on the strip
22. mousedownStrip() set the flag to true and enable the color changed
23. fillGradient() fill the color selector with the gradient
24. mousedown(e) enable to change the color of the brush
25. mousemove(e) change the color of the brush if it is enabled
26. mouseup(e) disable to change the color of brush
27. changeColor(e) change the color of the brush
   

### Gitlab page link

"https://108000219.gitlab.io/AS_01_WebCanvas"
    

### Others (Optional)

    

<style>
table th{
    width: 100%;
}
</style>
