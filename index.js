var canvas,ctx;
var drawingMode,changingColor,direction;
var cX,cY,pX,pY;
var w,h;
var colorBlock,colorStrip,colorLabel,backgroundLabel;
var ctx1,ctx2;
var LineWidth;
var width1,height1;
var width2,height2;
var x,y,drag,dragStrip;
var rgbaColor,grd1;
var colorofpen;
var drawingmode;
var PushArray;
var step;
var FlagText;
var restore,Savedata;
var input;
var fontsize,fontstyle;
var checkbox,checkbox1;
const curve = 0;
const rectangle = 1;
const circle = 2;
const triangle = 3;
const eraser = 4;
const text = 5;


function init(){
    
    PushArray = new Array();
    step = -1;
    canvas = document.getElementById('canvas');
    w=canvas.width;
    h=canvas.height;
    ctx = canvas.getContext("2d");
    ctx.strokeStyle = '#2196F3';
    ctx.lineJoin = 'round'; 
    ctx.lineCap = 'round';
    ctx.lineWidth = 5;
    drawingmode = curve;
    drawingMode = false;
    FlagText = false;
    fontsize = 24;
    fontstyle = "Arial";
    
    LineWidth = 2;
    cX = 0; //X coordinate
    cY = 0; //Y coordinate

    changingColor = 0; //color variable responsible for changing color
    direction = true; //changes the size of the brush
    colorofpen = "red";
    push();

    //color selector
    checkbox = document.getElementById("bgc");
    backgroundLabel = document.getElementById("BackgroundColor");
    colorLabel = document.getElementById('color-label');
    colorBlock = document.getElementById('color-block');

    ctx1 = colorBlock.getContext('2d');
    width1 = colorBlock.width;
    height1 = colorBlock.height;

    colorStrip = document.getElementById('color-strip');
    ctx2 = colorStrip.getContext('2d');
    width2 = colorStrip.width;
    height2 = colorStrip.height;

    x = 0;
    y = 0;
    drag = false;
    dragStrip=false;
    rgbaColor = 'rgba(255,0,0,1)';

    ctx1.rect(0, 0, width1, height1);
    fillGradient();

    ctx2.rect(0, 0, width2, height2);
    grd1 = ctx2.createLinearGradient(0, 0, 0, height1);
    grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
    grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
    grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
    grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
    grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
    grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
    grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
    ctx2.fillStyle = grd1;
    ctx2.fill();

    //bonus
    checkbox1 = document.getElementById("dwbgc");
}

function updatelinewidth(val){
    LineWidth = parseInt(val);
}

function changeBrush(num){
    drawingmode = num;
}
function push(){
    step++;
    if(step < PushArray.length){
        PushArray.length = step;
    }
    var data = document.getElementById("canvas").toDataURL("image/png",1);
    PushArray.push(data);
}

function draw(e) {    

    if(drawingmode == curve){
        document.getElementById("canvas").style.cursor = 'url("Pencil2.png"),auto'; 
    }else if(drawingmode == circle){
        document.getElementById("canvas").style.cursor = 'url("Circle2.png"),auto'; 
    }else if(drawingmode == rectangle){
        document.getElementById("canvas").style.cursor = 'url("Rectangle2.png"),auto'; 
    }else if(drawingmode == triangle){
        document.getElementById("canvas").style.cursor = 'url("Triangle2.png"),auto'; 
    }else if(drawingmode == eraser){
        document.getElementById("canvas").style.cursor = 'url("Eraser2.png"),auto'; 
    }else if(drawingmode == text){
        document.getElementById("canvas").style.cursor = 'text';
    }

    if (!drawingMode) return;

    if(drawingmode == curve){

        pX = cX;
        pY = cY;   
        cX = e.clientX - canvas.offsetLeft;
        cY = e.clientY - canvas.offsetTop;

        ctx.beginPath();
        ctx.moveTo(pX, pY);
        ctx.lineTo(cX, cY);
        ctx.strokeStyle = colorofpen ;
        ctx.lineWidth = LineWidth;
        ctx.stroke();

    }else if(drawingmode == circle){

        var radius;
        cX = e.clientX - canvas.offsetLeft;
        cY = e.clientY - canvas.offsetTop;

        ctx.beginPath();
        radius= Math.sqrt((cX-pX)*(cX-pX)/4+(cY-pY)*(cY-pY)/4);
        ctx.arc((cX+pX)/2,(cY+pY)/2,radius,0,2*Math.PI );
        ctx.strokeStyle = colorofpen ;
        ctx.lineWidth = LineWidth;
        ctx.clearRect(0, 0, w, h);
        ctx.drawImage(restore,0,0);
        ctx.stroke();

    }else if(drawingmode == rectangle){

        cX = e.clientX - canvas.offsetLeft;
        cY = e.clientY - canvas.offsetTop;

        ctx.beginPath();
        if(cX>pX&&cY>pY){
            ctx.rect(pX,pY,cX-pX,cY-pY);
        }else if(cX>pX&&cY<pY){
            ctx.rect(pX,cY,cX-pX,pY-cY);
        }else if(cX<pX&&cY>pY){
            ctx.rect(cX,pY,pX-cX,cY-pY);
        }else{
            ctx.rect(cX,cY,pX-cX,pY-cY);
        }

        ctx.strokeStyle = colorofpen ;
        ctx.lineWidth = LineWidth;
        ctx.clearRect(0, 0, w, h);
        ctx.drawImage(restore,0,0);
        ctx.stroke();

    }else if(drawingmode == triangle){

        cX = e.clientX - canvas.offsetLeft;
        cY = e.clientY - canvas.offsetTop;

        ctx.beginPath();
        ctx.moveTo(pX,pY);
        ctx.lineTo(pX,cY);
        ctx.lineTo(cX,cY);
        ctx.lineTo(pX,pY);
        ctx.strokeStyle = colorofpen ;
        ctx.lineWidth = LineWidth;
        ctx.clearRect(0, 0, w, h);
        ctx.drawImage(restore,0,0);
        ctx.stroke();

    }else if(drawingmode == eraser){
        cX = e.clientX - canvas.offsetLeft;
        cY = e.clientY - canvas.offsetTop;
        ctx.clearRect(cX,cY,LineWidth,LineWidth);
    }
      
}
function Mousedown(e){

    drawingMode = true;
    if(drawingmode == curve){

        pX = cX;
        pY = cY;        
        cX = e.clientX - canvas.offsetLeft;
        cY = e.clientY - canvas.offsetTop;

    }else if(drawingmode == circle||drawingmode == triangle ||drawingmode == rectangle){       
        
        pX = e.clientX - canvas.offsetLeft;
        pY = e.clientY - canvas.offsetTop;
        restore = new Image();
        restore.src = document.getElementById("canvas").toDataURL("image/png",1);
   
    }
    
}
function Mouseup(){

    if(drawingMode == true && drawingmode != text){
        push();
        drawingMode = false; 
    }

} 

function Mouseout(){
    
    if(drawingmode == curve || drawingMode == eraser){
        if(drawingMode)push();
    }
    drawingMode = false;
}

function Mouseclick(e){

    if(drawingmode != text)return;

    if(!FlagText){

        pX = e.clientX - canvas.offsetLeft;
        pY = e.clientY - canvas.offsetTop;
        fontsize = document.getElementById("FontSize").value;

        FlagText = true;
        input = document.createElement('input');
        input.type = 'text';
        input.id = "myTextbox";
        input.style = "position:fixed;font-size:"+fontsize.toString()+"px;";
        var X = e.clientX;
        var Y = e.clientY;
        input.style.left = X.toString()+"px";
        input.style.top = Y.toString()+"px";
        document.body.appendChild(input);
        
        input.focus();

    }else{

        var textboxx = document.getElementById("myTextbox");
        drawText(textboxx.value, pX, pY);
        document.body.removeChild(textboxx);
        FlagText = false;

    }

}

function drawText(txt, x, y) {

    fontsize = document.getElementById("FontSize").value;
    fontstyle = document.getElementById("FontStyle").value;
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = fontsize.toString()+"px "+fontstyle;
    ctx.fillText(txt,x, y);
    push();

}

function undo(){

    if(step>0){
        step--;
        var canvasPic = new Image();
        canvasPic.src = PushArray[step];
        canvasPic.onload = function(){
            ctx.clearRect(0, 0, w, h);
            ctx.drawImage(canvasPic,0,0);
        }
    }
    
}

function redo() {

    if (step < PushArray.length-1) {
        step++;
        var canvasPic = new Image();
        canvasPic.src = PushArray[step];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }

}

function onclear(){

    ctx.clearRect(0, 0, w, h);

}
function addBackgnd(){
    return new Promise((resolve,reject)=>{
        var canvasPic;
        canvasPic = new Image();
        canvasPic.src = canvas.toDataURL("image/png",0.8);
        ctx.fillStyle = backgroundLabel.style.backgroundColor;
        ctx.clearRect(0,0,w,h);
        ctx.fillRect(0,0,w,h);
        canvasPic.onload = function(){
            ctx.drawImage(this, 0, 0);
            push();
            setTimeout(() => {resolve("done");},10);
            Savedata = canvas.toDataURL("image/png",1); 
        } 
        setTimeout(() => {reject("failed");},15);
        
    });
}
async function onsave(){
    try{
        if(checkbox1.checked == true){
            await addBackgnd();
        }else{
            Savedata = canvas.toDataURL("image/png",1); 
        }
    }catch{
    }
   
    
    var anchor = document.createElement('a');
    anchor.href = Savedata;
    anchor.download = "image.png";
    anchor.click();
    if(checkbox1.checked == true){
        undo();
    }
}
function LoadImg(e){
    restore = new Image();
    restore.onload = Draw;
    restore.onerror = failed;
    restore.src = URL.createObjectURL(document.getElementById("inputfile").files[0]);
};
function Draw(){
    ctx.drawImage(this,0,0);
    push();
}
function failed(){
    alert("failed to upload the image");
}

//color selector

function mousemoveStrip(e) {
    
    if(!dragStrip)return;
    x = e.offsetX;
    y = e.offsetY;
    var imageData = ctx2.getImageData(x, y, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    if(checkbox.checked == false){
        colorLabel.style.backgroundColor = rgbaColor;
        colorofpen = rgbaColor;
    }else{
        backgroundLabel.style.backgroundColor = rgbaColor;
        canvas.style.background = rgbaColor;
    }
    
    fillGradient();

}
function mouseclickStrip(e){
    x = e.offsetX;
    y = e.offsetY;
    var imageData = ctx2.getImageData(x, y, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    if(checkbox.checked == false){
        colorLabel.style.backgroundColor = rgbaColor;
        colorofpen = rgbaColor;
    }else{
        backgroundLabel.style.backgroundColor = rgbaColor;
        canvas.style.background = rgbaColor;
    }
    fillGradient();
}
function mousedownStrip(){
    dragStrip = true;
}
function mouseupStrip(){
    dragStrip = false;
}
function fillGradient() {
    ctx1.fillStyle = rgbaColor;
    ctx1.fillRect(0, 0, width1, height1);

    var grdWhite = ctx2.createLinearGradient(0, 0, width1, 0);
    grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
    grdWhite.addColorStop(1, 'rgba(255,255,255,0)');

    ctx1.fillStyle = grdWhite;
    ctx1.fillRect(0, 0, width1, height1);

    var grdBlack = ctx2.createLinearGradient(0, 0, 0, height1);
    grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
    grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
    ctx1.fillStyle = grdBlack;
    //ctx1.fillRect(0, 0, width1, height1);
    ctx1.fill();
}

function mousedown(e) {
    drag = true;
    changeColor(e);
}

function mousemove(e) {
    if (drag) {
      changeColor(e);
    }
}

function mouseup(e) {
    drag = false;
}

function changeColor(e) {
    x = e.offsetX;
    y = e.offsetY;
    var imageData = ctx1.getImageData(x, y, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    if(checkbox.checked == false){
        colorLabel.style.backgroundColor = rgbaColor;
        colorofpen = rgbaColor;
    }else{
        backgroundLabel.style.backgroundColor = rgbaColor;
        canvas.style.background = rgbaColor;
    }
}

